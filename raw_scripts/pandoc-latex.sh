#!/bin/sh
sudo apt install pandoc texlive-latex-recommended texlive-xetex texlive-fonts-extra texlive-lang-german texlive-font-utils
cd $HOME
mkdir .pandoc/templates -p
cd .pandoc/templates 
curl -OL https://raw.githubusercontent.com/Wandmalfarbe/pandoc-latex-template/master/eisvogel.tex
curl -OL https://raw.githubusercontent.com/tajmone/pandoc-goodies/master/templates/html5/github/GitHub.html5
cd $HOME
pip install pandoc-latex-environment pandocfilters --user
