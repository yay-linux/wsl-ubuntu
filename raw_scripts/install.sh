# Some additional tools via apt
sudo apt update
sudo apt upgrade -y
sudo apt install ranger socat iproute2 ansible atool highlight curl plantuml pandoc gcc fortune-mod tldr ripgrep tree fzf pdftk-java -y

# Give Ranger some nice borders
ranger --copy-config=rc
#nano $HOME/.config/ranger/rc.conf
sed -i "s/set draw_borders none/set draw_borders both/g" $HOME/.config/ranger/rc.conf

# PPA for the latest neovim version
sudo add-apt-repository ppa:neovim-ppa/unstable
sudo apt update
sudo apt install neovim -y
# replace the next line if you have personal nvim config already
git clone https://github.com/gengor-git/neovim-config.git $HOME/.config/nvim

# Exchange to latest PlantUML jar
curl -OL https://github.com/plantuml/plantuml/releases/download/v1.2024.5/plantuml-pdf-1.2024.5.jar
sudo mv plantuml-pdf-1.2024.5.jar /usr/share/plantuml/plantuml.jar

# Python PIP for jupyter & mkdocs
sudo apt install python3-pip black python3-autopep8 python3-jedi python3-flake8 python3-yapf -y
pip install jupyterlab numpy seaborn pandas openpyxl matplotlib nbdime html5lib --user
pip install mkdocs mkdocs-material mkdocs-material-extensions mkdocs-build-plantuml-plugin --user

# Updated golang binaries
curl -OL https://dl.google.com/go/go1.22.4.linux-amd64.tar.gz
sudo rm -rf /usr/local/go && sudo tar -C /usr/local -xzf go1.22.4.linux-amd64.tar.gz

# BAT is like cat but cooler with more UI but it has an issue when installing in
# Ubuntu parallel to ripgrep. So we have to install it manually.
curl -OL https://github.com/sharkdp/bat/releases/download/v0.24.0/bat_0.24.0_amd64.deb
sudo dpkg -i bat_0.24.0_amd64.deb

# If you want a current NodeJS, uncomment the next block
sudo apt-get install -y ca-certificates curl gnupg
#sudo mkdir -p /etc/apt/keyrings
#curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
#NODE_MAJOR=20
#echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | sudo tee /etc/apt/sources.list.d/nodesource.list
#sudo apt-get update
#sudo apt-get install nodejs -y

# Z-Shell for more convenience
sudo apt install zsh -y
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
# Activate plugins in .zshrc
sed -i "s/plugins=(git)/plugins=(git zsh-autosuggestions zsh-syntax-highlighting copypath copyfile)/g" $HOME/.zshrc

# Install SSH pageant bridge
# https://github.com/BlackReloaded/wsl2-ssh-pageant/
mkdir -p $HOME/.ssh
windows_destination="/mnt/c/Users/Public/Downloads/wsl2-ssh-pageant.exe"
linux_destination="$HOME/.ssh/wsl2-ssh-pageant.exe"
if [ -e "$windows_destination" ]; then
  echo "wsl2-ssh-pageant.exe file exists, skipping download."
else
  wget -O "$windows_destination" "https://github.com/BlackReloaded/wsl2-ssh-pageant/releases/latest/download/wsl2-ssh-pageant.exe"
fi
# Set the executable bit.
sudo chmod +x "$windows_destination"
# Symlink to linux for ease of use later
ln -s $windows_destination $linux_destination
cat "wsl2-ssh-pageant-config-zsh.txt" >> $HOME/.zshrc
echo "export PATH=\$PATH:\$HOME/.local/bin:\$HOME/go/bin:/usr/local/go/bin:$HOME/node_modules/.bin" >> $HOME/.zshrc
export PATH=$PATH:/usr/local/go/bin:$HOME./local/bin:$HOME/go/bin
go install github.com/jesseduffield/lazygit@latest
go install github.com/xxxserxxx/gotop/v4/cmd/gotop@latest
#nano $HOME/.zshrc

