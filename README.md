# WSL-Ubuntu

This project is for my personal Ubuntu setup in Windows Subsystem for Linux
(WSL).

It uses Ansible playbooks to install additional software and make configurations
and changes to the system.

alternatively you can run a bash script (only use after clean wsl reset), which offers even more changes.

This repo is currently in early stages and should be considered unstable.

In other words: don't use it in productive or otherwise important environments!
You do use it at your own risk.


## Structure

```
├── LICENSE
├── README.md
├── ansible
│   ├── config.yml          -> configure what to install
│   ├── install-sudo.yml    -> install the system level
│   ├── install-user.yml    -> install the user level items
│   ├── inventory           -> localhost
│   └── run.sh              -> run this for your convenience
├── raw_scripts
│   ├── install.sh          -> Alternative to Ansible with more customizations
│   ├── pandoc-latex.sh     -> LaTeX and pip packages for Pandoc
│   └── wsl2-ssh-pageant-config-zsh.txt
└── windows_host
   ├── idea.ps1             -> Install Intellij IDEA portable
   └── vscode-setup.ps1     -> Install VS Code extensions
```
