#!/bin/bash
sudo apt update
sudo apt upgrade -y
sudo apt install ansible git -y
sudo ansible-playbook -i inventory install-sudo.yml
ansible-playbook -i inventory install-user.yml
