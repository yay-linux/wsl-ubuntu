<#
Pageant is part of the PuTTY utils and can be used as an SSH agent inside of
windows.
https://www.chiark.greenend.org.uk/~sgtatham/putty/
#>

$PageantExe = "C:\Program Files\PuTTY\pageant.exe"

<#
If you want to use pageant also for OpenSSH, you need pageant to export an
OpenSSH config upon launch and then have it included in you .ssh/config file
like this:

Include pageant.conf
#>
$PageantParameters = "--openssh-config " + $env:USERPROFILE + "\.ssh\pageant.conf"
$SshDirectory = "$env:USERPROFILE\.ssh"
$PlinkExe = "C:\Program Files\PuTTY\plink.exe"

if (-not (Test-Path $SshDirectory)) {
    New-Item -Path $SshDirectory -ItemType directory
}


if (Test-Path $PageantExe) {
    # let's create a shortcut in the autostart menu
    $ShortcutPath = "$env:USERPROFILE\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\Pageant - PuTTY SSH agent.lnk"
    $Shell = New-Object -ComObject ("WScript.Shell")
    if (Test-Path $ShortcutPath) {
        Remove-Item -Path $ShortcutPath -Force
    }
    $PageantShortcut = $Shell.CreateShortcut($ShortcutPath)
    $PageantShortcut.TargetPath = $PageantExe
    $PageantShortcut.Arguments = "$PageantParameters"
    $PageantShortcut.WorkingDirectory = "C:\Program Files\PuTTY";
    $PageantShortcut.WindowStyle = 3;
    $PageantShortcut.Hotkey = "";
    $PageantShortcut.IconLocation = "$PageantExe, 0";
    $PageantShortcut.Description = "Pageant - PuTTY SSH agent";
    $PageantShortcut.Save()

    # now let's connect GIT to Pageant via an Environment variable that is
    # linked to plink (which is also part of the PuTTY utils).

    $EnvGitSsh = [System.Environment]::GetEnvironmentVariable("GIT_SSH", "User")
    $EnvGitSsh = $PlinkExe
    if (-not ($dryrun)) { [System.Environment]::SetEnvironmentVariable("GIT_SSH", $EnvGitSsh, "User") }
}
else {
    Write-Warning "Pageant not found. Won't do any changes."
}