﻿$IdeaDownloadUrl = 'https://download-cdn.jetbrains.com/idea/ideaIC-2022.2.win.zip'
$IdeaZipFilename = $IdeaDownloadUrl.Split("/")[-1]
$IdeaWebHash = "27f7c793f328cfebcbdb1f4bd036bb157d1d69c70226816f4872435885a1a685"
$PortablePath = "C:\Portable"
$IdeaPath = "$PortablePath\intellij-community"

Set-Location $env:USERPROFILE\Downloads

if (-not (Test-Path $IdeaZipFilename)) {
  & curl.exe -OL $IdeaDownloadUrl
}

$DownloadHash = (Get-FileHash $env:USERPROFILE\Downloads\$IdeaZipFilename).hash

# Only install if the hash of the download matches the one I originally got from the homepage.
if ($DownloadHash -eq $IdeaWebHash) {
  if (-not (Test-Path $PortablePath)) {
    Write-Host "Portable will be created."
    New-Item $PortablePath -ItemType Directory
  }

  if (-not (Test-Path $IdeaPath)) {
    Write-Host "IntelliJ folder will be created."
    New-Item $IdeaPath -ItemType Directory

    if (Test-Path "C:\Program Files\7-Zip\7z.exe") {
      Start-Process -FilePath "C:\Program Files\7-Zip\7z.exe" -ArgumentList "x $env:USERPROFILE\Downloads\$IdeaZipFilename -o$IdeaPath" -Wait
    }
    else {
      Expand-Archive $env:USERPROFILE\Downloads\$IdeaZipFilename -DestinationPath $IdeaPath -Verbose
    }

    if (Test-Path "$PortablePath\intellij-community\bin\idea64.exe") {
      # let's create a shortcut in the start menu
      $ShortcutPath = "$env:USERPROFILE\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\IntelliJ IDEA Community Portable.lnk"
      $Shell = New-Object -ComObject ("WScript.Shell")
      if (Test-Path $ShortcutPath) {
        Remove-Item -Path $ShortcutPath -Force
      }
      $IntellijShortcut = $Shell.CreateShortcut($ShortcutPath)
      $IntellijShortcut.TargetPath = "$IdeaPath\bin\idea64.exe"
      $IntellijShortcut.Arguments = ""
      $IntellijShortcut.WorkingDirectory = $IdeaPath;
      $IntellijShortcut.WindowStyle = 3;
      $IntellijShortcut.Hotkey = "";
      $IntellijShortcut.IconLocation = "$IdeaPath\bin\idea.ico, 0";
      $IntellijShortcut.Description = "IntelliJ IDEA Community Portable";
      $IntellijShortcut.Save()
    }
  }
  else {
    Write-Warning "Could not install. Check if folder exists already."
  }
}
else {
  Write-Warning "Could not install. File-Hash does not match."
  Write-Warning " Expected $IdeaWebHash"
  Write-Warning "      Was $DownloadHash"
}